extends Node2D


func _ready():
	pass

func _on_TB_Play_pressed():
	get_tree().change_scene("res://Scene/Game.tscn")


func _on_TB_Credits_pressed():
	get_tree().change_scene("res://Scene/Credits.tscn")


func _on_TB_Quit_pressed():
	get_tree().quit()
