extends Node2D
#print(VI_Base_Form.size())

#Variables de Scenas Inicio
var Lbl_Life #Para Manejar el texto (Dentro del Icono Corazon)
var Lbl_Time #Para Manejar el texto (Debajo del Icono reloj)
var Lbl_Question #Para Manejar el Texto (Dentro del Cuadrado de Pregunta)
var Lbl_Puntos #Para Manejar el Texto (Debajo de la Palabra Score)
var Lbl_Score_Final #Para Manejar el Texto (La Palabra Score al finalizar el juego)
var Lbl_Option1 #Para Manejar el Texto dentro del Boton (Boton 1)
var Lbl_Option2 #Para Manejar el Texto dentro del Boton (Boton 2)
var Lbl_Option3 #Para Manejar el Texto dentro del Boton (Boton 3)
#Variables de Scenas Fin

#Variable de Palabras Inicio
  #Verbos Regulares
var VR_Base_Form=["Accept","Act","Achieve","Admire","Advice","Affect","Agree","Amaze","Amuse","Answer","Appear","Arrange","Arrive","Ask","Attack","Bake","Behave","Believe","Belong","Blame","Borrow","Bother","Call","Cancel","Carry","Cause","Celebrate","Clean","Clear","Climb","Close","Compare","Compete","Complete","Contain","Continue","Cook","Correct","Cough","Count","Crash","Create","Cross","Curse","Change","Chase","Chat","Check","Damage","Dance","Date","Decide","Deliver","Depend","Describe","Design","Destroy","Dicrease","Die","Disagree","Discover","Discuss","Disturb","Dress","Dry","Eliminate","End","Enjoy","Entertain","Excuse","Exercise","Exhibit","Expect","Express","Film","Fill","Fish","Fix","Follow","Freeze","Fry","Greet","Guess","Hail","Handle","Happed","Hate","Help","Hope","Hunt","Identify","Ignore","Imagine","Impress","Improve","Include","Increase","Interview","Introduce","Invite","Jog","Join","Jump","Knock","Label","Land","Last","Learn","Like","Link","List","Listen","Live","Locate","Look","Love","Manage","Mark","Match","Measure","Mention","Miss","Move","Name","Need","Note","Notice","Number","Offer","Open","Order","Organize","Pack","Paint","Pamper","Pardon","Park","Participate","Pass","Perform","Persuade","Pick","Plan","Play","Please","Practice","Predict","Prefer","Present","Program","Protect","Provide","Purchase","Push","Rain","Receive","Recommend","Relate","Relax","Release","Remember","Repair","Repeat","Resist","Rest","Return","Review","Sail","Save","Scan","Scare","Share","Shop","Shout","Skate","Ski","Slow","Sneeze","Snow","Solve","Spell","Start","Step","Stop","Stress","Study","Substitute","Suggest","Surprise","Talk","Taste","Terrorize","Thank","Touch","Travel","Try","Tune","Turn","Underline","Use","Vary","Wait","Walk","Want","Warn","Wash","Watch","Water","Welcome","Wish","Witness","Work","Worry","Wrestle"]
var VR_Past_Simple=["Accepted","Acted","Achieved","Admired","Adviced","Affected","Agreed","Amazed","Amused","Answered","Appeared","Arranged","Arrived","Asked","Attacked","Baked","Behaved","Believed","Belonged","Blamed","Borrowed","Bothered","Called","Canceled","Carried","Caused","Celebrated","Cleaned","Cleared","Climbed","Closed","Compared","Competed","Completed","Contained","Continued","Cooked","Corrected","Coughed","Counted","Crashed","Created","Crossed","Cursed","Changed","Chased","Chatted","Checked","Damaged","Danced","Dated","Decided","Delivered","Depended","Described","Designed","Destroyed","Dicreased","Died","Disagreed","Discovered","Discussed","Disturbed","Dressed","Dried","Eliminated","Ended","Enjoyed","Entertained","Excused","Exercised","Exhibited","Expected","Expressed","Filmed","Filled","Fished","Fixed","Followed","Freezed","Fried","Greeted","Guessed","Hailed","Handled","Happened","Hated","Helped","Hoped","Hunted","Identified","Ignored","Imagined","Impressed","Improved","Included","Increased","Interviewed","Introduced","Invited","Jogged","Joined","Jumped","Knocked","Labeled","Landed","Lasted","Learned","Liked","Linked","Listed","Listened","Lived","Located","Looked","Loved","Managed","Marked","Matched","Measured","Mentioned","Missed","Moved","Named","Needed","Noted","Noticed","Numbered","Offered","Opened","Ordered","Organized","Packed","Painted","Pampered","Pardoned","Parked","Participated","Passed","Performed","Persuaded","Picked","Planned","Played","Pleased","Practiced","Predicted","Preferred","Presented","Programmed","Protected","Provided","Purchased","Pushed","Rained","Received","Recommended","Related","Relaxed","Released","Remembered","Repaired","Repeated","Resisted","Rested","Returned","Reviewed","Sailed","Saved","Scanned","Scared","Shared","Shopped","Shouted","Skated","Skied","Slowed","Sneezed","Snowed","Solved","Spelled","Started","Stepped","Stopped","Stressed","Studied","Substituted","Suggested","Surprised","Talked","Tasted","Terrorized","Thanked","Touched","Traveled","Tried","Tuned","Turned","Underlined","Used","Varied","Waited","Walked","Wanted","Warned","Washed","Watched","Watered","Welcomed","Wished","Witnessed","Worked","Worried","Wrestled"]
var VR_Traduccion=["Aceptar","Actuar","Lograr,Alcanzar","Admirar","Aconsejar","Afectar","Acordar","Asombrar","Divertir","Responder","Aparecer","Arreglar,Disponer","Llegar","Preguntar","Atacar","Hornear","Comportarse","Creer","Pertenecer","Culpar","Pedir prestado","Molestar","Llamar","Cancelar,Suprimir","Llevar","Causar","Celebrar","Limpiar","Aclarar","Trepar","Cerrar","Comparar","Competir","Completar","Contener","Continuar","Cocinar","Corregir","Toser","Contar","Chocar","Crear","Cruzar","Maldecir","Cambiar","Perseguir","Charlar","Verificar","Dañar","Bailar","Salir con,Fecha","Decidir","Entregar","Depender","Describir","Diseñar","Destruir","Disminuir","Morir","Estar en desacuerdo","Descubrir","Discutir","Molestar","Vestir","Secar","Eliminar","Terminar","Disfrutar","Entretener","Excusar","Ejercitar","Exhibir","Esperar","Expresar","Filmar","Llenar","Pescar","Arreglar,Fijar","Seguir","Congelar","Freir","Saludar","Adivinar,Suponer","Granizar","Manejar","Suceder","Odiar,Cargarle a uno","Ayudar","Esperar","Cazar","Identificar","Ignorar","Imaginar","Impresionar","Mejorar","Incluir","Aumentar","Entrevistar","Introducir,Presentar","Invitar","Trotar","Juntar,Unir,Acompañar","Saltar","Golpear","Rotular,Etiquetar","Aterrizar","Durar","Aprender","Gustarle a uno","Unir, Vincular","Listar","Escuchar","Vivir","Ubicar","Mirar,Parecer","Amar,Encantarle a uno","Manejar,Dirigir","Marcar","Unir,Aparejar","Medir","Mencionar","Perderse,Echar de menos,Perder","Mover,Trasladar,Cambiarse","Nombrar","Necesitar","Notar","Notar,Fijarse","Numerar","Ofrecer","Abrir","Ordenar","Organizar","Empacar","Pintar","Regalonear","Perdonar","Estacionar","Participar","Pasar,Aprobar","Realizar,Ejecutar","Persuadir","Recoger","Planear","Jugar,Tocar","Complacer","Practicar","Predecir","Preferir","Presentar","Programar","Proteger","Proporcionar","Comprar","Empujar","Llover","Recibir","Recomendar","Relacionar","Descansar","Soltar,Producir","Recordar","Reparar","Repetir","Resistir","Descansar","Volver","Revisar","Navegar","Ahorrar,Salvar","Examinar","Asustar","Compartir","Comprar","Gritar","Patinar","Esquiar","Disminuir","Estornudar","Nevar","Resolver","Deletrear","Empezar","Pisar","Detener","Acentuar","Estudiar","Sustituir","Sugerir","Sorprender","Hablar","Probar","Aterrorizar","Agradecer","Tocar","Viajar","Tratar","Sintonizar","Girar","Subrayar","Usar","Variar","Esperar","Caminar","Desear","Advertir","Lavar","Ver,Vigilar","Regar","Recibir","Desear","Ser testigo","Trabajar,Funcionar","Preocuparse","Luchar"]
  #Verbos Irregulares
var VI_Base_Form=["Arise","Awake","Be/am,are,is","Bear","Beat","Become","Begin","Bend","Bet","Bind","Bid","Bite","Bleed","Blow","Break","Breed","Bring","Broadcast","Build","Burn","Burst","Buy","Cast","Catch","Come","Cost","Cut","Choose","Cling","Creep","Deal","Dig","Do (Does)","Draw","Dream","Drink","Drive","Eat","Fall","Feed","Feel","Fight","Find","Flee","Fly","Forbid","Forget","Forgive","Freeze","Get","Give","Go (Goes)","Grow","Grind","Hang","Have","Hear","Hide","Hit","Hold","Hurt","Keep","Know","Kneel","Knit","Lay","Lead","Lean","Leap","Learn","Leave","Lend","Let","Lie","Light","Lose","Make","Mean","Meet","Mistake","Overcome","Pay","Put","Read","Ride","Ring","Rise","Run","Say","See","Seek","Sell","Send","Set","Sew","Shake","Shear","Shine","Shoot","Show","Shrink","Shut","Sing","Sink","Sit","Sleep","Slide","Smell","Sow","Speak","Speed","Spell","Spend","Spill","Spin","Spit","Split","Spoil","Spread","Spring","Stand","Steal","Stick","Sting","Stink","Stride","Strike","Swear","Sweat","Sweep","Swell","Swim","Swing","Take","Teach","Tear","Tell","Think","Throw","Thrust","Tread","Understand","Undergo","Undertake","Wake","Wear","Weave","Weep","Wet","Wind","Wind","Withdraw","Wring","Write"]
var VI_Past_Simple=["Arose","Awoke","Was / Were","Bore","Beat","Became","Began","Bent","Bet","Bound","Bid","Bit","Bled","Blew","Broke","Bred","Brought","Broadcast","Built","Burnt/Burned","Burst","Bought","Cast","Caught","Came","Cost","Cut","Chose","Clung","Crept","Dealt","Dug","Did","Dreaw","Dreamt/Dreamed","Drank","Drove","Ate","Fell","Fed","Felt","Fought","Found","Fled","Flew","Forbade","Forgot","Forgave","Froze","Got","Gave","Went","Grew","Ground","Hung","Had","Heard","Hid","Hit","Held","Hurt","Kept","Knew","Knelt","Knit","Laid","Led","Leant","Leapt","Learnt/Learned","Left","Lent","Let","Lay","Lit","Lost","Made","Meant","Met","Mistook","Overcame","Paid","Put","Read","Rode","Rang","Rose","Ran","Said","Saw","Sought","Sold","Sent","Set","Sewed","Shook","Shore","Shone","Shot","Showed","Shrank","Shut","Sang","Sank","Sat","Slept","Slid","Smelt","Sowed","Spoke","Sped","Spelt","Spent","Spilt/Spilled","Spun","Spat","Split","Spoilt/Spoiled","Spread","Sprang","Stood","Stole","Stuck","Stung","Stank/Stunk","Strode","Struck","Swore","Sweat","Swept","Swelled","Swam","Swung","Took","Taught","Tore","Told","Thought","Threw","Thrust","Trod","Understood","Underwent","Undertook","Woke","Wore","Wove","Wept","Wet","Won","Wound","Withedrew","Wrung","Wrote"]
var VI_Part_Participle=["Arisen","Awoken","Been","Borne/Born","Beaten","Become","Begun","Bent","Bet","Bound","Bid","Bitten","Bled","Blown","Broken","Bred","Brought","Broadcast","Built","Burnt/Burned","Burst","Bought","Cast","Caught","Come","Cost","Cut","Chosen","Clung","Crept","Dealt","Dug","Done","Drawn","Dreamt/Dreamed","Drunk","Driven","Eaten","Fallen","Fed","Felt","Fought","Found","Fled","Flown","Forbidden","Forgotten","Forgiven","Frozen","Got/Gotten","Given","Gone","Grown","Ground","Hung","Had","Heard","Hidden","Hit","Held","Hurt","Kept","Known","Knelt","Knit","Laid","Led","Leant","Leapt","Learnt/Learned","Left","Lent","Let","Lain","Lit","Lost","Made","Meant","Met","Mistaken","Overcome","Paid","Put","Read","Ridden","Rung","Risen","Rung","Said","Seen","Sought","Sold","Sent","Set","Sewed/Sewn","Shaken","Shorn","Shone","Shot","Shown","Shrunk","Shut","Sung","Sunk","Sat","Slept","Slid","Smelt","Sowed/Sown","Spoken","Sped","Spelt","Spent","Spilt/Spilled","Spun","Spat","Split","Spoilt/Spoiled","Spread","Sprung","Stood","Stolen","Stuck","Stung","Stunk","Stridden","Struck","Sworn","Sweat","Swept","Swollen","Swum","Swung","Taken","Taught","Torn","Told","Thought","Thrown","Thrust","Trodden","Understood","Undergone","Undertaken","Woken","Worn","Woven","Wept","Wet","Won","Wound","Withdrawn","Wrung","Written"]
var VI_Traduccion=["Surgir,Levantarse","Despertarse","Ser/Estar","Soportar,Dar a luz","Golpear","Llegar a ser","Empezar","Doblar","Apostar","Atar,Encuadernar","Pujar","Morder","Sangrar","Soplar","Romper","Criar","Traer,Llevar","Radiar","Edificar","Quemar","Reventar","Comprar","Arrojar","Coger","Venir","Costar","Cortar","Elegir","Agarrarse","Arrastrarse","Tratar","Cavar","Hacer","Dibujar","Soñar","Beber","Conducir","Comer","Caer","Alimentar","Sentir","Luchar","Encontrar","Huir","Volar","Prohibir","Olvidar","Perdonar","Helar","Obtener","Dar","Ir","Crecer","Moler","Colgar","Haber o Tener","Oir","Ocultar","Golpear","Agarrar,Celebrar","Herir","Conservar","Saber,Conocer","Arrodillarse","Hacer punto","Poner","Conducir","Apoyarse","Brincar","Aprender","Dejar","Prestar","Permitir","Echarse","Encender","Perder","Hacer","Significar","Encontrar","Equivocar","Vencer","Pagar","Poner","Leer","Montar","Llamar","Levantarse","Correr","Decir","Ver","Buscar","Vender","Enviar","Poner (se)","Coser","Sacudir","Esquilar","Brillar","Disparar","Mostrar","Encogerse","Cerrar","Cantar","Hundir","Sentarse","Dormir","Resbalar","Oler","Sembrar","Hablar","Acelerar","Deletrear","Gastar","Derramar","Hilar","Escupir","Hender/Partir/Rajar","Estropear","Extender","Saltar","Estar en pie","Robar","Pegar,Engomar","Picar","Apestar","Dar zancadas","Golpear","Jurar","Sudar","Barrer","Hinchar","Nadar","Columpiarse","Coger","Enseñar","Rasgar","Decir","Pensar","Arrojar/Tirar","Introducir","Pisar,Hollar","Entender","Sufrir","Emprender","Despertarse","Llevar puesto","Tejer","Llorar","Mojar","Ganar","Enrollar","Retirarse","Torcer","Escribir"]
  #Variable de Palabras Fin

#Variable de Seleccion de Pregunta Inicio
var Pregunta_Tipo=0
    #si es 0 = Verbo de Ingles a español
    #si es 1 = Verbo de Ingles a pasado simple
    #si es 2 = Verbo de Ingles a pasado participio
#Variable de Seleccion de Pregunta Fin

#Variable de Seleccion Tipo de verbo Inicio
var TipoVerb=0
   #Si es = 0 Se activan lo verbos Regulares
   #Si es = 1 Se activan lo verbos Irregulares
#Variable de Seleccion Tipo de verbo Fin

#Variable de llenado con las palabras Seleccionadas Inicio
var Palabras_SelectP=[""] #Palabra Seleccionada para guardar Pregunta
var Palabras_SelectR=[""] #Palabra Seleccionada para guardar Respuesta
#Variable de llenado con las palabras Seleccionadas Fin

#Variable Tamaño de la Matriz Inicio
var Size_Matriz=0 #Guardara el tamaño de la matriz
#Variable Tamaño de la Matriz Fin

#Variables de Seleccion de opciones Inicio
var N_Palabra=0 #Se guardara el Numero de la palabra random que hay en la matriz
var C_Palabra=1 #Contador para saber cuantas palabras ya lleno la matriz de abajo
var Matriz_Palabras=[0,0,0] #Se guardaran los numeros de palabras random seleccionadas y mostradas en los botones
var NS_Palabra=0 #Se guardara el numero de respuesta de la matriz
#Variables de Seleccion de opciones Fin

#Variables de Texto Pregunta Inicio
var Palabra_Pregunta=""
#Variables de Texto Pregunta Fin

#Variables Interfaz Inicio
var Vida=3
var Score=0
var Tiempo=20
var Presiono=false
#Variables Interfaz Fin

#Funcion ready es la primera cosa que se inicia al cargar la escena
func _ready():
	randomize() #Determina que un numero no debe ser igual a una partida anterior

#Aqui se cargaran en las "Variables de Scenas" los objetos dentro de la escena a la que pertenece cada uno

#Cargado de Variables Inicio
	Lbl_Life=$UI_Icons/Life/Lbl_Life
	Lbl_Time=$UI_Icons/Time/Lbl_Time
	Lbl_Question=$UI_Icons/Question/Lbl_Question
	Lbl_Puntos=$UI_Icons/Lbl_Puntos
	Lbl_Score_Final=$Final/Background_Final/Lbl_Score_Final
	Lbl_Option1=$UI_Buttons/TB_Option1/Lbl_Option1
	Lbl_Option2=$UI_Buttons/TB_Option2/Lbl_Option2
	Lbl_Option3=$UI_Buttons/TB_Option3/Lbl_Option3
#Cargado de Variables Fin

#Llama a la funcion "TipoPreguntaAzar"
	_TipoPreguntaAzar()

#Funcion donde se Elijira de manera random un Tipo de Pregunta
func _TipoPreguntaAzar():
	Pregunta_Tipo=randi()%3 #Devuelve un numero random del 0 al 2 guardandolo en "Pregunta_Tipo"

#Condicional para mandar a una funcion dependiendo el tipo de pregunta
	if(Pregunta_Tipo==0):
		_Verb_Ing_Es() #Si Pregunta Tipo = 0 enviara a la funcion Verbo de Ingles a Español
	if(Pregunta_Tipo==1):
		_Verb_Ing_PasSimple() #Si Pregunta Tipo = 1 enviara a la funcion Verbo de Ingles a Pasado Simple
	if(Pregunta_Tipo==2):
		_Verb_Ing_PastParticiple() #Si Pregunta Tipo = 2 enviara a la funcion Verbo de Ingles a Pasado Participio

#Funcion donde el Tipo de Pregunta sera Verbo de Ingles a Español
func _Verb_Ing_Es():
	_Verb_Tipo() #Envia a la funcion "_Verb_Tipo()"
	#Condicion para llenar con verbo regulares o irregulares 
	if(TipoVerb==0):
		Size_Matriz=214 #Las palabras en los verbos regulares son 214 aqui se define eso
		Palabras_SelectP=VR_Base_Form #Llena la variable de "Palabras_SelectP" con lo que contiene "VR_Base_Form"
		Palabras_SelectR=VR_Traduccion #Llena la variable de "Palabras_SelectR" con lo que contiene "VR_Traduccion"
	else:
		Size_Matriz=152 #Las palabras en los verbos irregulares son 152 aqui se define eso
		Palabras_SelectP=VI_Base_Form #Llena la variable de "Palabras_SelectP" con lo que contiene "VI_Base_Form"
		Palabras_SelectR=VI_Traduccion #Llena la variable de "Palabras_SelectR" con lo que contiene "VI_Traduccion"
	_Seleccionar_Verbos()

#Funcion donde el Tipo de Pregunta sera Verbo de Ingles a Pasado Simple
func _Verb_Ing_PasSimple():
	_Verb_Tipo() #Envia a la funcion "_Verb_Tipo()"
	#Condicion para llenar con verbo regulares o irregulares 
	if(TipoVerb==0):
		Size_Matriz=214 #Las palabras en los verbos regulares son 214 aqui se define eso
		Palabras_SelectP=VR_Base_Form #Llena la variable de "Palabras_SelectP" con lo que contiene "VR_Base_Form"
		Palabras_SelectR=VR_Past_Simple #Llena la variable de "Palabras_SelectR" con lo que contiene "VR_Traduccion"
	else:
		Size_Matriz=152 #Las palabras en los verbos irregulares son 152 aqui se define eso
		Palabras_SelectP=VI_Base_Form #Llena la variable de "Palabras_SelectP" con lo que contiene "VI_Base_Form"
		Palabras_SelectR=VI_Past_Simple #Llena la variable de "Palabras_SelectR" con lo que contiene "VI_Traduccion"
	_Seleccionar_Verbos()

#Funcion donde el Tipo de Pregunta sera Verbo de Ingles a Pasado Participio
func _Verb_Ing_PastParticiple():
	_Verb_Tipo() #Envia a la funcion "_Verb_Tipo()"
	#Condicion para llenar con verbo regulares o irregulares 
	if(TipoVerb==0):
		Size_Matriz=214 #Las palabras en los verbos regulares son 214 aqui se define eso
		Palabras_SelectP=VR_Base_Form #Llena la variable de "Palabras_SelectP" con lo que contiene "VR_Base_Form"
		Palabras_SelectR=VR_Past_Simple #Llena la variable de "Palabras_SelectR" con lo que contiene "VR_Traduccion"
	else:
		Size_Matriz=152 #Las palabras en los verbos irregulares son 152 aqui se define eso
		Palabras_SelectP=VI_Base_Form #Llena la variable de "Palabras_SelectP" con lo que contiene "VI_Base_Form"
		Palabras_SelectR=VI_Part_Participle #Llena la variable de "Palabras_SelectR" con lo que contiene "VI_Traduccion"
	_Seleccionar_Verbos()

#Funcion para saber que tipo de verbo se utilizara
func _Verb_Tipo():
	TipoVerb=randi()%2 #Devuelve un numero ramdon de 0 a 1 guardandolo en "TipoVerb"

func _Seleccionar_Verbos():
	N_Palabra=randi()%Size_Matriz
	if (C_Palabra==1):
		Matriz_Palabras[0]=N_Palabra
		C_Palabra=C_Palabra+1
		_Seleccionar_Verbos()
	else:
		if(C_Palabra==2):
			Matriz_Palabras[1]=N_Palabra
			C_Palabra=C_Palabra+1
			_Seleccionar_Verbos()
		else:
			Matriz_Palabras[2]=N_Palabra
			C_Palabra=1
			NS_Palabra=randi()%Matriz_Palabras.size()
			_Cargado_Botones()

func _Cargado_Botones():
	Lbl_Option1.set_text(str(Palabras_SelectR[Matriz_Palabras[0]]))
	Lbl_Option2.set_text(str(Palabras_SelectR[Matriz_Palabras[1]]))
	Lbl_Option3.set_text(str(Palabras_SelectR[Matriz_Palabras[2]]))
	Palabra_Pregunta=Palabras_SelectP[Matriz_Palabras[NS_Palabra]]
	#print(Matriz_Palabras[0],"-",Matriz_Palabras[1],"-",Matriz_Palabras[2])
	#print((Palabras_SelectP[Matriz_Palabras[0]]),"-",(Palabras_SelectP[Matriz_Palabras[1]]),"-",(Palabras_SelectP[Matriz_Palabras[2]]))
	#print((Palabras_SelectR[Matriz_Palabras[0]]),"-",(Palabras_SelectR[Matriz_Palabras[1]]),"-",(Palabras_SelectR[Matriz_Palabras[2]]))
	$Timer.start()
	if(Pregunta_Tipo==0):
		Lbl_Question.set_text("Traduce el siguiente Verbo: "+Palabra_Pregunta)
	if(Pregunta_Tipo==1):
		Lbl_Question.set_text("Pasado simple del siguiente verbo: "+Palabra_Pregunta)
	if(Pregunta_Tipo==2):
		Lbl_Question.set_text("Pasado Participio del siguiente verbo: "+Palabra_Pregunta)


func _on_Timer_timeout():
	if(Tiempo==21):
		$Audio_Fondo.set_pitch_scale(1)
	if(Tiempo>0 and Tiempo<21):
		Tiempo=Tiempo-1
		Lbl_Time.set_text(str(Tiempo))
	if (Tiempo==10):
		$Audio_Fondo.set_pitch_scale(1.3) 
	if(Tiempo==0):
		$Audio_Fondo.set_pitch_scale(1)
		$Timer.stop()
		if(Presiono==false):
			_Quita_Vida()

func _Quita_Vida():
	if(Vida>1):
		Vida=Vida-1
		Tiempo=20
		_TipoPreguntaAzar()
	else:
		if(Vida==1):
			$Final/Timer_Final.start()
			$Final.show()
			Lbl_Score_Final.set_text("Your score is: "+Lbl_Puntos.get_text())
			#get_tree().change_scene("res://Scene/Inicio.tscn")
	Lbl_Life.set_text(str(Vida))

func _on_TB_Option1_pressed():
	Tiempo=20
	Lbl_Time.set_text(str(Tiempo))
	Presiono=true
	if(NS_Palabra==0 and Presiono==true):
		Presiono=false
		_Aumentar_Score()
		_TipoPreguntaAzar()
		_Cargado_Audio("Bien")
	else:
		_Quita_Vida()
		_Cargado_Audio("Mal")


func _on_TB_Option2_pressed():
	Tiempo=20
	Lbl_Time.set_text(str(Tiempo))
	Presiono=true
	if(NS_Palabra==1 and Presiono==true):
		Presiono=false
		_Aumentar_Score()
		_TipoPreguntaAzar()
		_Cargado_Audio("Bien")
	else:
		_Quita_Vida()
		_Cargado_Audio("Mal")


func _on_TB_Option3_pressed():
	Tiempo=20
	Lbl_Time.set_text(str(Tiempo))
	Presiono=true
	if(NS_Palabra==2 and Presiono==true):
		Presiono=false
		_Aumentar_Score()
		_TipoPreguntaAzar()
		_Cargado_Audio("Bien")
	else:
		_Quita_Vida()
		_Cargado_Audio("Mal")

func _Aumentar_Score():
	Score=Score+1
	Lbl_Puntos.set_text(str(Score))

func _on_Timer_Final_timeout():
	$Final.hide()
	get_tree().change_scene("res://Scene/Inicio.tscn")

func _Cargado_Audio(Tipo):
	if(Tipo=="Bien"):
		$Audio_Effects.set_stream(load("res://Sound/Respuesta_Bien.wav"))
		$Audio_Effects.play(0.1)
	if(Tipo=="Mal"):
		$Audio_Effects.set_stream(load("res://Sound/Respuesta_Mal.wav"))
		$Audio_Effects.play(0.1)